       IDENTIFICATION DIVISION.
       PROGRAM-ID. PRODUCT-INQUIRY.
       AUTHOR. SEMPIO, JETHRO KYLE B.
       DATE-WRITTEN. MARCH 21, 2018
       DATE-COMPILED.

       ENVIRONMENT DIVISION.
       
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

		       SELECT PRD-FILE
             ASSIGN TO DISK 
             ORGANIZATION IS INDEXED
             ACCESS MODE IS DYNAMIC
             RECORD KEY IS PRD-ID
             FILE STATUS IS PRD-STAT.       	   

           SELECT ITM-FILE
             ASSIGN TO DISK 
             ORGANIZATION IS INDEXED
             ACCESS MODE IS DYNAMIC
             RECORD KEY IS ITM-ID
             FILE STATUS IS ITM-STAT.

       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.

       DATA DIVISION.
       FILE SECTION.

       FD PRD-FILE
              LABEL RECORDS ARE STANDARD
              VALUE OF FILE-ID IS 'PRD.DAT'.

       01 PRD-RECORD.
           02 PRD-ID         PIC X(5).
           02 PRD-NAME       PIC X(20).
           02 PRD-COST       PIC 9999999V99.
           02 PRD-ITM OCCURS 5 TIMES.
              05 PRD-FK-ITM-ID PIC X(5).
              05 PRD-ITM-QTY   PIC 99999999.

       FD ITM-FILE
              LABEL RECORDS ARE STANDARD
              VALUE OF FILE-ID IS
              'C:\PROJECT\ITEM\ITM.DAT'.

       01 ITM-RECORD.
           02 ITM-ID         PIC X(5).
           02 ITM-NAME       PIC X(20).
           02 QTY-UOM        PIC X(10).
           02 ITM-COST       PIC 9999999V99.
           02 ITM-QTY        PIC 9999999.
           02 ITM-CAT        PIC X(3).
           02 ITM-TYPE       PIC 9.
           02 ITM-DATE.
              05 DATE-M       PIC 99.
              05 DATE-D       PIC 99.
              05 DATE-Y       PIC 9999.
           02 ITM-EXP        PIC 9(10).
           02 EXP-UOM        PIC X(10).
           02 FK-SUP-ID         PIC X(5).

       WORKING-STORAGE SECTION.
       01 WS-PRD-RECORD.
           02 WS-PRD-ID         PIC X(5).
           02 WS-PRD-NAME       PIC X(20).
           02 WS-PRD-COST       PIC 9999999V99.
           02 WS-PRD-ITM OCCURS 5 TIMES.
              05 WS-PRD-FK-ITM-ID PIC X(5).
              05 WS-PRD-ITM-QTY   PIC 9.
       77 PRD-STAT              PIC XX.

       01 WS-ITM-RECORD.
           02 WS-ITM-ID         PIC X(5).
           02 WS-ITM-NAME       PIC X(20).
           02 WS-QTY-UOM        PIC X(10).
           02 WS-ITM-COST       PIC 9999999V99.
           02 WS-ITM-QTY        PIC 9999999.
           02 WS-ITM-CAT        PIC X(3).
           02 WS-ITM-TYPE       PIC 9.
           02 WS-ITM-DATE.
              05 WS-DATE-M       PIC 99.
              05 WS-DATE-D       PIC 99.
              05 WS-DATE-Y       PIC 9999.
           02 WS-ITM-EXP     PIC 9(10).
           02 WS-EXP-UOM        PIC X(10).
           02 WS-FK-SUP-ID         PIC X(5).
       77 ITM-STAT              PIC XX.

       77 ISFOUND                  PIC 9.
       77 OPT                      PIC X.
       77 ISLOOP                   PIC 9 VALUE 1.
       77 COUNTER 				   PIC 99 VALUE 1.


       SCREEN SECTION.
       01 ENTER-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³T-SHIRT PRINTING INVT.   ³".
           02 LINE 12 COLUMN 30 "³PRODUCT INQUIRY          ³".
           02 LINE 13 COLUMN 30 "³                         ³".
           02 LINE 14 COLUMN 30 "³                         ³".
           02 LINE 15 COLUMN 30 "³     ENTER PRD-ID        ³".
           02 LINE 16 COLUMN 30 "³         *****           ³".
           02 LINE 17 COLUMN 30 "³         ÄÄÄÄÄ           ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³                         ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³                         ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".

       01 DATA1-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³PRD ID:               1/1³".
           02 LINE 12 COLUMN 30 "ÃÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ´".
           02 LINE 13 COLUMN 30 "³PRODUCT NAME:            ³".
           02 LINE 14 COLUMN 30 "³                         ³".
           02 LINE 15 COLUMN 30 "³COST EACH:               ³".
           02 LINE 16 COLUMN 30 "³                         ³".
           02 LINE 17 COLUMN 30 "³                         ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³                         ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³                         ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".
       
       01 DATA2-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³ITEMS CONSUMED        2/2³".
           02 LINE 12 COLUMN 30 "³ITM-ID:***** QTY:        ³".
           02 LINE 13 COLUMN 30 "³#1:                      ³".
           02 LINE 14 COLUMN 30 "³                         ³".
           02 LINE 15 COLUMN 30 "³#2:                      ³".
           02 LINE 16 COLUMN 30 "³                         ³".
           02 LINE 17 COLUMN 30 "³#3:                      ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³#4:                      ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³#5:                      ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".

       01 CONFIRM-SAVE-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³                         ³".
           02 LINE 12 COLUMN 30 "³                         ³".
           02 LINE 13 COLUMN 30 "³                         ³".
           02 LINE 14 COLUMN 30 "³          SAVE?          ³".
           02 LINE 15 COLUMN 30 "³         ( Y/N )         ³".
           02 LINE 16 COLUMN 30 "³                         ³".
           02 LINE 17 COLUMN 30 "³                         ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³                         ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³                         ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".

       01 REQUERY-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³                         ³".
           02 LINE 12 COLUMN 30 "³                         ³".
           02 LINE 13 COLUMN 30 "³                         ³".
           02 LINE 14 COLUMN 30 "³   QUERY PRODUCT AGAIN?  ³".
           02 LINE 15 COLUMN 30 "³          (Y/N)          ³".
           02 LINE 16 COLUMN 30 "³                         ³".
           02 LINE 17 COLUMN 30 "³                         ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³                         ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³                         ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".
       
       01 X-SCREEN.
           02 LINE 10 COLUMN 30 "ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿".
           02 LINE 11 COLUMN 30 "³                         ³".
           02 LINE 12 COLUMN 30 "³                         ³".
           02 LINE 13 COLUMN 30 "³                         ³".
           02 LINE 14 COLUMN 30 "³                         ³".
           02 LINE 15 COLUMN 30 "³                         ³".
           02 LINE 16 COLUMN 30 "³                         ³".
           02 LINE 17 COLUMN 30 "³                         ³".
           02 LINE 18 COLUMN 30 "³                         ³".
           02 LINE 19 COLUMN 30 "³                         ³".
           02 LINE 20 COLUMN 30 "³                         ³".
           02 LINE 21 COLUMN 30 "³                         ³".
           02 LINE 22 COLUMN 30 "³                         ³".
           02 LINE 23 COLUMN 30 "ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ".

       01 CLEAR-SCREEN.
           05 BLANK SCREEN.

       PROCEDURE DIVISION.
       MAIN-PROGRAM.
           PERFORM ENTER-PRD-ID UNTIL ISLOOP = 0.
           DISPLAY X-SCREEN.
           STOP RUN.

       OPEN-PRD-FILE.
           OPEN I-O PRD-FILE.
           IF PRD-STAT NOT = '00'
              OPEN OUTPUT PRD-FILE
              CLOSE PRD-FILE
              OPEN I-O PRD-FILE.

       OPEN-ITM-FILE.
           OPEN I-O ITM-FILE.
           IF ITM-STAT NOT = '00'
              OPEN OUTPUT ITM-FILE
              CLOSE ITM-FILE
              OPEN I-O ITM-FILE.

       ENTER-PRD-ID.
           PERFORM CLEAR-PRD-FIELDS.
           DISPLAY CLEAR-SCREEN.
           DISPLAY ENTER-SCREEN.
           ACCEPT (16, 40) WS-PRD-ID.
           PERFORM SEARCH-PRODUCT.    
            
       SEARCH-PRODUCT.
           PERFORM OPEN-PRD-FILE.
           MOVE WS-PRD-ID TO PRD-ID.
           MOVE 1 TO ISFOUND.
           READ PRD-FILE RECORD
           INVALID KEY
           MOVE 0 TO ISFOUND.
              
           IF ISFOUND = 0
              PERFORM ENTER-PRODUCT
           ELSE IF ISFOUND = 1
              PERFORM SHOW-PRODUCT.
           CLOSE PRD-FILE.
           DISPLAY CLEAR-SCREEN.
           DISPLAY REQUERY-SCREEN.
           MOVE SPACES TO OPT.
           ACCEPT (22, 31) OPT.
           IF OPT = 'Y' OR OPT = 'y'
              MOVE 1 TO ISLOOP
           ELSE
              MOVE 0 TO ISLOOP.

       SAVE-PRD-ITEMS.
           MOVE WS-PRD-FK-ITM-ID(COUNTER) TO PRD-FK-ITM-ID(COUNTER).
           MOVE WS-PRD-ITM-QTY(COUNTER) TO PRD-ITM-QTY(COUNTER).
           ADD 1 TO COUNTER.

       SAVE-PRODUCT.
           MOVE WS-PRD-ID    TO PRD-ID.
           MOVE WS-PRD-NAME  TO PRD-NAME.
           MOVE WS-PRD-COST  TO PRD-COST.
           MOVE 1 TO COUNTER.
           PERFORM SAVE-PRD-ITEMS UNTIL COUNTER > 5.
           MOVE 1 TO COUNTER.
           WRITE PRD-RECORD.

       CLEAR-PRD-ITEMS.
           MOVE SPACES TO WS-PRD-FK-ITM-ID(COUNTER).
           MOVE ZEROES TO WS-PRD-ITM-QTY(COUNTER).
           ADD 1 TO COUNTER.

       CLEAR-PRD-FIELDS.
           MOVE SPACES TO WS-PRD-ID.
           MOVE SPACES TO WS-PRD-NAME.
           MOVE ZEROES TO WS-PRD-COST.
           MOVE 1 TO COUNTER.
           PERFORM CLEAR-PRD-ITEMS UNTIL COUNTER > 5.
           MOVE 1 TO COUNTER.

       SEARCH-ITEMS.
       	   MOVE PRD-FK-ITM-ID(COUNTER) TO ITM-ID.
       	   MOVE 1 TO ISFOUND.
       	   READ ITM-FILE RECORD
       	   INVALID KEY
       	   MOVE 0 TO ISFOUND.

       	   IF ISFOUND = 0
       	      MOVE 99999 TO ITM-ID
       	      READ ITM-FILE RECORD
       	      INVALID KEY
       	      MOVE 0 TO ISFOUND.
       	   MOVE 1 TO ISFOUND.

       SHOW-ITEMS.
           MOVE 0 TO ISFOUND.
           PERFORM OPEN-ITM-FILE.
       	   PERFORM SEARCH-ITEMS.

       	   IF COUNTER = 1
       	      DISPLAY (13, 34) ITM-ID
       	      DISPLAY (14, 31) ITM-NAME
       	      DISPLAY (13, 41) "QTY:" PRD-ITM-QTY(COUNTER).

       	   IF COUNTER = 2
       	      DISPLAY (15, 34) ITM-ID
       	      DISPLAY (16, 31) ITM-NAME
       	      DISPLAY (15, 41) "QTY:" PRD-ITM-QTY(COUNTER).

       	   IF COUNTER = 3
       	      DISPLAY (17, 34) ITM-ID
       	      DISPLAY (18, 31) ITM-NAME
       	      DISPLAY (17, 41) "QTY:" PRD-ITM-QTY(COUNTER).

       	   IF COUNTER = 4
       	      DISPLAY (19, 34) ITM-ID
       	      DISPLAY (20, 31) ITM-NAME
       	      DISPLAY (19, 41) "QTY:" PRD-ITM-QTY(COUNTER).

       	   IF COUNTER = 5
       	      DISPLAY (21, 34) ITM-ID
       	      DISPLAY (22, 31) ITM-NAME
       	      DISPLAY (21, 41) "QTY:" PRD-ITM-QTY(COUNTER).
       	   
       	   ADD 1 TO COUNTER.
       	   CLOSE ITM-FILE.

       SHOW-PRODUCT.
       	   DISPLAY CLEAR-SCREEN.
           DISPLAY DATA1-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)"
           DISPLAY (11, 39) PRD-ID.
           DISPLAY (14, 31) PRD-NAME.
           DISPLAY (18, 31) PRD-COST.
           ACCEPT (22, 48) OPT.

           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA2-SCREEN.
           DISPLAY (12, 30) "³                         ³".
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".
          
           MOVE 1 TO COUNTER.
           PERFORM SHOW-ITEMS UNTIL COUNTER > 5.
           MOVE 1 TO COUNTER.
           ACCEPT (22, 48) OPT.