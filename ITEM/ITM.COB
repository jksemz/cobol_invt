       FD ITM-FILE
       LABEL RECORDS ARE STANDARD
       VALUE OF FILE-ID IS 'ITM.DAT'.

       01 ITM-RECORD.
           02 ITM-ID         PIC X(5).
           02 ITM-NAME       PIC X(20).
           02 QTY-UOM        PIC X(10).
           02 ITM-COST       PIC 9999999V99.
           02 ITM-QTY        PIC 9999999.
           02 ITM-CAT        PIC X(3).
           02 ITM-TYPE       PIC 9.
           02 ITM-DATE.
              05 DATE-M       PIC 99.
              05 DATE-D       PIC 99.
              05 DATE-Y       PIC 9999.
           02 ITM-EXP        PIC 9(10).
           02 EXP-UOM        PIC X(10).
           02 SUP-ID         PIC X(5).