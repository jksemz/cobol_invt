       IDENTIFICATION DIVISION.
       PROGRAM-ID.  EMPLOYEE-ENTRY.
       AUTHOR. SEMPIO, JETHRO KYLE B.
       DATE-WRITTEN. FEB 5, 2018
       DATE-COMPILED.

       ENVIRONMENT DIVISION.
       
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT EMP-FILE
             ASSIGN TO DISK 
             ORGANIZATION IS INDEXED
             ACCESS MODE IS DYNAMIC
             RECORD KEY IS EMP-ID
             FILE STATUS IS EMP-STAT.

       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.

       DATA DIVISION.
       FILE SECTION.
       FD EMP-FILE
              LABEL RECORDS ARE STANDARD
              VALUE OF FILE-ID IS 'EMP.DAT'.

       01 EMP-RECORD.
           02 EMP-ID         PIC X(5).
           02 EMP-DEPT       PIC 9.
           02 EMP-POS        PIC X(15).
           02 EMP-WAGE       PIC 999999V99.
           02 EMP-NAME.
              05 EMP-FNAME   PIC X(40).
              05 EMP-LNAME   PIC X(40).
              05 EMP-MI      PIC X.
           02 EMP-ADDR.
              05 ADDR-NUM          PIC X(5).
              05 ADDR-STREET       PIC X(20).
              05 ADDR-BRGY         PIC X(15).
              05 ADDR-CITY         PIC X(15).
              05 ADDR-PROV         PIC X(15).
              05 ADDR-COUNTRY      PIC X(15).
              05 ADDR-ZIP          PIC 9(4).
           02 EMP-CONTACT         PIC X(11).

       WORKING-STORAGE SECTION.
       01 WS-EMP-RECORD.
           02 WS-EMP-ID         PIC X(5)        VALUE SPACES.
           02 WS-EMP-DEPT       PIC 9           VALUE ZEROES.
           02 WS-EMP-POS        PIC X(15)       VALUE SPACES. 
           02 WS-EMP-WAGE       PIC 999999V99   VALUE ZEROES.
           02 WS-EMP-NAME.
              05 WS-EMP-FNAME   PIC X(40)       VALUE SPACES.
              05 WS-EMP-LNAME   PIC X(40)       VALUE SPACES.
              05 WS-EMP-MI      PIC X           VALUE SPACES.
           02 WS-EMP-ADDR.
              05 WS-ADDR-NUM          PIC X(5)  VALUE SPACES.
              05 WS-ADDR-STREET       PIC X(20) VALUE SPACES.
              05 WS-ADDR-BRGY         PIC X(15) VALUE SPACES.
              05 WS-ADDR-CITY         PIC X(15) VALUE SPACES.
              05 WS-ADDR-PROV         PIC X(15) VALUE SPACES.
              05 WS-ADDR-COUNTRY      PIC X(15) VALUE SPACES.
              05 WS-ADDR-ZIP          PIC 9(4)  VALUE ZEROES.
           02 WS-EMP-CONTACT         PIC X(11)  VALUE ZEROES.
          01 EMP-STAT                PIC XX.
          77 ISFOUND                  PIC 9.
          77 OPT                      PIC X.
          77 ISLOOP                   PIC 9 VALUE 1.

       SCREEN SECTION.
       01 ENTER-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿟-SHIRT PRINTING INVT.   �".
           02 LINE 12 COLUMN 30 "쿐MPLOYEE ENTRY           �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "�     ENTER EMP-ID        �".
           02 LINE 16 COLUMN 30 "�         *****           �".
           02 LINE 17 COLUMN 30 "�         컴컴�           �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 DATA1-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿐MP ID:               1/3�".
           02 LINE 12 COLUMN 30 "쳐컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 13 COLUMN 30 "쿒IVEN NAME:              �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "쿗AST NAME:               �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "쿘I:                      �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "쿎ONTACT NO:              �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 DATA2-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿕OB DESCRIPTION       2/3�".
           02 LINE 12 COLUMN 30 "쳐컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 13 COLUMN 30 "쿏EPARTMENT:              �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "쿛OSITION:                �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "쿞ALARY:                  �".
           02 LINE 18 COLUMN 30 "쿛HP                      �".
           02 LINE 19 COLUMN 30 "쳐컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 20 COLUMN 30 "쿏EPARTMENTS   5-STOCK    �".
           02 LINE 21 COLUMN 30 "�1-ACC 3-SALES 6-MNG      �".
           02 LINE 22 COLUMN 30 "�2-MNF 4-DLVY  7-OTHER    �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 DATA3-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿌DDRESS               3/3�".
           02 LINE 12 COLUMN 30 "쳐컴컴컴컴컴컫컴컴컴컴컴컴�".
           02 LINE 13 COLUMN 30 "쿙UMBER:     쿎OUNTRY:    �".
           02 LINE 14 COLUMN 30 "�            �            �".
           02 LINE 15 COLUMN 30 "쿞TREET:     쿩IP:        �".
           02 LINE 16 COLUMN 30 "�            �            �".
           02 LINE 17 COLUMN 30 "쿍RGY:       �            �".
           02 LINE 18 COLUMN 30 "�            �            �".
           02 LINE 19 COLUMN 30 "쿎ITY:       �            �".
           02 LINE 20 COLUMN 30 "�            �            �".
           02 LINE 21 COLUMN 30 "쿛ROV:       �            �".
           02 LINE 22 COLUMN 30 "�            �            �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컨컴컴컴컴컴컴�".

       01 CONFIRM-SAVE-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�          SAVE?          �".
           02 LINE 15 COLUMN 30 "�         ( Y/N )         �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 REQUERY-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�  QUERY EMPLOYEE AGAIN?  �".
           02 LINE 15 COLUMN 30 "�          (Y/N)          �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".
       
       01 X-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "�                         �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 CLEAR-SCREEN.
           05 BLANK SCREEN.

       PROCEDURE DIVISION.
       MAIN-PROGRAM.
           PERFORM ENTER-EMP-ID UNTIL ISLOOP = 0.
           DISPLAY X-SCREEN.
           STOP RUN.

       OPEN-FILE.
           OPEN I-O EMP-FILE.
           IF EMP-STAT NOT = '00'
              OPEN OUTPUT EMP-FILE
              CLOSE EMP-FILE
              OPEN I-O EMP-FILE.

       ENTER-EMP-ID.
           DISPLAY CLEAR-SCREEN.
           DISPLAY ENTER-SCREEN.
           ACCEPT (16, 40) WS-EMP-ID.
           PERFORM SEARCH-EMPLOYEE.    
            
       SEARCH-EMPLOYEE.
           PERFORM OPEN-FILE. 
           MOVE WS-EMP-ID TO EMP-ID.
           MOVE 1 TO ISFOUND
           READ EMP-FILE RECORD
           INVALID KEY
           MOVE 0 TO ISFOUND.
              
           IF ISFOUND = 0
              PERFORM ENTER-EMPLOYEE
           ELSE IF ISFOUND = 1
              PERFORM SHOW-EMPLOYEE.
           CLOSE EMP-FILE.
           DISPLAY CLEAR-SCREEN.
           DISPLAY REQUERY-SCREEN.
           MOVE SPACES TO OPT.
           ACCEPT (22, 31) OPT.
           IF OPT = 'Y' OR OPT = 'y'
              MOVE 1 TO ISLOOP
           ELSE
              MOVE 0 TO ISLOOP.

       SAVE-EMPLOYEE.
           MOVE WS-EMP-ID       TO EMP-ID.
           MOVE WS-EMP-DEPT     TO EMP-DEPT. 
           MOVE WS-EMP-POS      TO EMP-POS.
           MOVE WS-EMP-WAGE     TO EMP-WAGE.
           MOVE WS-EMP-FNAME    TO EMP-FNAME.
           MOVE WS-EMP-LNAME    TO EMP-LNAME.
           MOVE WS-EMP-MI       TO EMP-MI.
           MOVE WS-EMP-CONTACT  TO EMP-CONTACT.
           MOVE WS-ADDR-NUM     TO ADDR-NUM.
           MOVE WS-ADDR-STREET  TO ADDR-STREET.
           MOVE WS-ADDR-BRGY    TO ADDR-BRGY.
           MOVE WS-ADDR-CITY    TO ADDR-CITY.
           MOVE WS-ADDR-PROV    TO ADDR-PROV.
           MOVE WS-ADDR-COUNTRY TO ADDR-COUNTRY.
           MOVE WS-ADDR-ZIP     TO ADDR-ZIP.    
           WRITE EMP-RECORD.

       CLEAR-EMP-FIELDS.
           MOVE SPACES TO WS-EMP-ID.
           MOVE ZEROES TO WS-EMP-DEPT. 
           MOVE SPACES TO WS-EMP-POS.
           MOVE ZEROES TO WS-EMP-WAGE.
           MOVE SPACES TO WS-EMP-FNAME.
           MOVE SPACES TO WS-EMP-LNAME.
           MOVE SPACES TO WS-EMP-MI.   
           MOVE SPACES TO WS-EMP-CONTACT.
           MOVE SPACES TO WS-ADDR-NUM.
           MOVE SPACES TO WS-ADDR-STREET.
           MOVE SPACES TO WS-ADDR-BRGY.
           MOVE SPACES TO WS-ADDR-CITY.
           MOVE SPACES TO WS-ADDR-PROV.
           MOVE SPACES TO WS-ADDR-COUNTRY.
           MOVE ZEROES TO WS-ADDR-ZIP.  
       
       SHOW-EMPLOYEE.
           DISPLAY DATA1-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".

           DISPLAY (11, 39) EMP-ID.
           DISPLAY (14, 31) EMP-FNAME.
           DISPLAY (16, 31) EMP-LNAME.
           DISPLAY (17, 34) EMP-MI.
           DISPLAY (20, 31) EMP-CONTACT.
           ACCEPT (22, 48) OPT.
           
           DISPLAY CLEAR-SCREEN.

           DISPLAY DATA2-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".

           DISPLAY (14, 31) EMP-DEPT.
           DISPLAY (16, 31) EMP-POS.
           DISPLAY (18, 35) EMP-WAGE.
           ACCEPT (22, 48) OPT.

           DISPLAY CLEAR-SCREEN.

           DISPLAY DATA3-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".

           DISPLAY (14, 31) ADDR-NUM.
           DISPLAY (16, 31) ADDR-STREET.
           DISPLAY (18, 31) ADDR-BRGY.
           DISPLAY (20, 31) ADDR-CITY.
           DISPLAY (22, 31) ADDR-PROV.
           DISPLAY (14, 44) ADDR-COUNTRY.
           DISPLAY (16, 44) ADDR-ZIP.
           ACCEPT (22, 48) OPT.       

       ENTER-EMPLOYEE.
           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA1-SCREEN.
           DISPLAY (11, 39) EMP-ID.
           ACCEPT (14, 31) WS-EMP-FNAME.
           ACCEPT (16, 31) WS-EMP-LNAME.
           ACCEPT (17, 34) WS-EMP-MI.
           ACCEPT (20, 31) WS-EMP-CONTACT.

           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA2-SCREEN.
           ACCEPT (14, 31) WS-EMP-DEPT.
           ACCEPT (16, 31) WS-EMP-POS.
           ACCEPT (18, 35) WS-EMP-WAGE.
           
           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA3-SCREEN.
           ACCEPT (14, 31) WS-ADDR-NUM.
           ACCEPT (16, 31) WS-ADDR-STREET.
           ACCEPT (18, 31) WS-ADDR-BRGY.
           ACCEPT (20, 31) WS-ADDR-CITY.
           ACCEPT (22, 31) WS-ADDR-PROV.
           ACCEPT (14, 44) WS-ADDR-COUNTRY.
           ACCEPT (16, 44) WS-ADDR-ZIP.

           DISPLAY CLEAR-SCREEN.
           DISPLAY CONFIRM-SAVE-SCREEN.
           MOVE SPACES TO OPT.
           ACCEPT (22, 31) OPT.
           IF OPT = 'Y' OR OPT = 'y'
              PERFORM SAVE-EMPLOYEE
           ELSE
              PERFORM CLEAR-EMP-FIELDS.
